$("#autocomplete-form").autocomplete ({
  source: function (req, res) {
    $.ajax({
      url: '/potepan/suggests',
      type: 'GET',
      cache: false,
      dataType: "json",
      data: { keyword: req.term },
      success: function (data) {
        res(data);
      },
      error: function (xhr, ts, err) {
        res(xhr, ts, err);
      }
    });
  }
});

$('.dropdown').change(function () {
  $(this).addClass('open');
});