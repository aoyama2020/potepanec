require 'api_suggest'
class Potepan::SuggestsController < ApplicationController
  SUGGEST_MAX_COUNT = 5
  def search
    suggests = ApiSuggest.suggest(params[:keyword], SUGGEST_MAX_COUNT)

    render body: suggests.body, status: suggests.code
  end
end
