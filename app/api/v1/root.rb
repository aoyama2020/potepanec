require 'api_potepan_suggest'
module V1
  class Root < Grape::API
    API_KEY = Rails.application.credentials.api[:API_KEY]
    format :json
    version 'suggests'

    params do
      requires :keyword, type: String, allow_blank: true
      optional :max_num, type: Integer
    end
    get '/' do
      TOKEN_PREFIX = /^Bearer\s/.freeze
      TOKEN_REGEX = /^Bearer\s(.+)/.freeze
      header_auth = headers['Authorization']
      if header_auth.match(TOKEN_REGEX) && header_auth.gsub(TOKEN_PREFIX, '') == API_KEY
        ApiPotepanSuggest.suggest(params[:keyword], suggest_max_count: params[:max_num])
      else
        error!('Unauthorized', 401)
      end
    end
  end
end
