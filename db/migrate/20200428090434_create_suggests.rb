class CreateSuggests < ActiveRecord::Migration[5.2]
  def change
    create_table :suggests do |t|
      t.string :keyword, null: false

      t.timestamps
    end
    add_index :suggests, :keyword, unique: true
  end
end
