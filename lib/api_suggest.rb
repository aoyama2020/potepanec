require 'httpclient'

class ApiSuggest
  API_KEY = Rails.application.credentials.api[:API_KEY]
  POTEPAN_API_URI = "https://presite-potepanec-task5.herokuapp.com/potepan/api/suggests".freeze

  def self.suggest(keyword, suggest_max_count)
    headers = {
      Authorization: "Bearer #{API_KEY}",
    }
    params = {
      keyword: keyword,
      max_num: suggest_max_count,
    }

    client = HTTPClient.new
    client.get(POTEPAN_API_URI, body: params, header: headers)
  end
end
