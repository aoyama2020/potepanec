class ApiPotepanSuggest
  def self.suggest(keyword, suggest_max_count: nil)
    result = Potepan::Suggest.where('keyword like ?', "#{keyword}%")
    result = result.limit(suggest_max_count)  if suggest_max_count.to_i >= 1
    result.pluck(:keyword)
  end
end
