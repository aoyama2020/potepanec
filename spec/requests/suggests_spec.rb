require 'rails_helper'
require 'httpclient'
require 'webmock/rspec'

RSpec.describe "Suggests", type: :request do
  let(:url) { "https://presite-potepanec-task5.herokuapp.com/potepan/api/suggests" }
  let(:http) { HTTPClient.new }
  let(:res) { http.get url }

  describe "リクエスト成功時" do
    before do
      WebMock.stub_request(:get, url).
        to_return(
          body: ["ruby", "ruby for women", "ruby for men", "rails", "rails for women"],
          status: 200,
          headers: { 'Content-Type' => 'application/json' }
        )
    end

    it 'statuscodeが200となる' do
      expect(res.status).to eq 200
    end

    it 'suggestの配列が返される' do
      expect(res.body).to eq ["ruby", "ruby for women", "ruby for men", "rails", "rails for women"]
    end
  end

  describe "リクエスト失敗時(ステータスコード401)" do
    before do
      WebMock.stub_request(:get, url).
        to_return(
          body: [],
          status: 401,
          headers: { 'Content-Type' => 'application/json' }
        )
    end

    it 'statuscodeが401となる' do
      expect(res.status).to eq 401
    end

    it 'suggestの配列が空で返される' do
      expect(res.body).to eq []
    end
  end
end
