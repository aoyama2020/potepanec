require 'rails_helper'

RSpec.describe "API", type: :request do
  let!(:words_starting_with_ruby) { create_list(:potepan_suggest, 6, :ruby) }
  let!(:word_starting_with_rails) { create(:potepan_suggest, :rails) }
  let!(:api_key) { Rails.application.credentials.api[:API_KEY] }
  let!(:params) { { keyword: 'ruby', max_num: 5 } }
  let(:headers) { { 'Authorization': "Bearer #{api_key}" } }

  describe "GET /api/suggests" do
    subject do
      get '/api/suggests', params: params, headers: headers
      response
    end

    context '認証成功、キーワードがrubyの場合' do
      it "リクエストが成功する" do
        is_expected.to be_successful
        is_expected.to have_http_status(:success)
        expect(subject.status).to eq 200
      end

      it "max_numで指定された5件を上限に配列を返す" do
        expect(JSON.parse(subject.body).size).to eq 5
      end

      it "指定したキーワード(ruby)から始まる配列のみを返す" do
        expect(JSON.parse(subject.body)).to all(match(/^ruby.*/i))
        expect(JSON.parse(subject.body)).not_to include word_starting_with_rails.keyword
      end
    end

    context '認証に失敗した場合' do
      let!(:api_key) { "invalid" }

      it "リクエストが失敗する" do
        is_expected.not_to be_successful
        is_expected.to have_http_status(:unauthorized)
        expect(subject.status).to eq 401
      end

      it "配列は返さず、エラーメッセージのみを返す" do
        expect(JSON.parse(subject.body)).to eq({ "error" => "Unauthorized" })
      end
    end
  end
end
