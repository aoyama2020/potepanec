require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "リクエストに成功する" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "商品名が含まれる" do
      expect(response.body).to include(product.name)
    end

    it "商品詳細が含まれる" do
      expect(response.body).to include(product.description)
    end

    it "商品価格が含まれる" do
      expect(response.body).to include(product.price.to_s)
    end

    it "関連商品情報が含まれる" do
      expect(response.body).to include(related_product.name)
      expect(response.body).to include(related_product.price.to_s)
    end
  end
end
