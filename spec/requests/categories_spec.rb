require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:other_taxon) do
      create(:taxon, parent_id: taxonomy.root.id,
                     taxonomy: taxonomy, name: "Ruby")
    end
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product, taxons: [other_taxon], price: "99.99") }

    before do
      get potepan_category_path(taxon.id)
    end

    it "リクエストに成功する" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "taxon,taxonomy名が含まれる" do
      expect(response.body).to include(taxon.name)
      expect(response.body).to include(taxonomy.name)
    end

    it "商品名が含まれる" do
      expect(response.body).to include(product.name)
    end

    it "異なるtaxonの商品名・価格が含まれない" do
      expect(response.body).not_to include(other_product.name)
      expect(response.body).not_to include(other_product.price.to_s)
    end
  end
end
