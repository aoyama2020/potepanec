require 'rails_helper'

RSpec.feature "CategoryPage", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "カテゴリ毎商品一覧ページ" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "各カテゴリ情報が含まれる" do
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(taxon.name)
      expect(page).to have_content(taxon.all_products.size)
    end

    it "HOMEページへのリンクをクリック" do
      click_on 'HOME'
      expect(current_path).to eq potepan_path
      expect(page).to have_title 'BIGBAG Store'
    end

    it "商品詳細ページへのリンク画像をクリック" do
      click_on "products-img-#{product.name}"
      expect(current_path).to eq potepan_product_path(product.id)
      expect(page).to have_title "#{product.name} - BIGBAG Store"
    end

    it "各カテゴリへのリンクをクリック" do
      click_on "#{taxon.name} (#{taxon.all_products.size})"
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    end
  end
end
