require 'rails_helper'

RSpec.feature "ProductPage", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe "商品詳細ページ" do
    before do
      visit potepan_product_path(product.id)
    end

    it "各商品詳細情報が含まれる" do
      expect(page).to have_content(product.name)
      expect(page).to have_content(product.description)
      expect(page).to have_content(product.price.to_s)
    end

    it "HOMEページへのリンクをクリック" do
      click_on 'HOME'
      expect(current_path).to eq potepan_path
      expect(page).to have_title 'BIGBAG Store'
    end

    it "商品一覧ページへのリンクをクリック" do
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(product.taxons.ids.first)
      expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    end

    it "関連商品詳細ページへのリンク画像をクリック" do
      click_on "products-img-#{related_product.name}"
      expect(current_path).to eq potepan_product_path(related_product.id)
      expect(page).to have_title "#{related_product.name} - BIGBAG Store"
    end

    it "関連商品が4つ以内で表示されている" do
      within ".productsContent" do
        expect(page).to have_selector("img", count: 4)
      end
    end
  end
end
