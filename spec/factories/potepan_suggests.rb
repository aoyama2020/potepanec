FactoryBot.define do
  factory :potepan_suggest, class: "Potepan::Suggest" do
    trait :ruby do
      sequence(:keyword, "ruby-keyword_1")
    end

    trait :rails do
      keyword { "rails-keyword" }
    end
  end
end
