require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let!(:product) { create(:product) }

  describe 'full_titleヘルパー' do
    it { expect(full_title(nil)).to eq "BIGBAG Store" }
    it { expect(full_title('')).to eq "BIGBAG Store" }
    it { expect(full_title(product.name)).to eq "#{product.name} - BIGBAG Store" }
  end
end
